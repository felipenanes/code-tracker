package com.pluralsight.nanes.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {

	@RequestMapping("/greeting")
	public String sayHello(Model model) {
		model.addAttribute("greeting", "Hello World");
		/* Retorna p�gina HTML que vai renderizar model(acima) na p�gina como objeto dela */
		return "hello";
	}

}
